import React from "react";
import {Modal, Form, Container, Button, Row, Col} from "react-bootstrap";
import './ModalForm.css'

class ModalFormPersonal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    onChangeNameForm = (e) =>    {
        this.props.setParentState({element: {...this.props.selectedItem, name: e.target.value}});
    }
    onChangeSurnameForm = (e) =>    {
        this.props.setParentState({element: {...this.props.selectedItem, surname: e.target.value}});
    }
    onChangeTeleForm = (e) =>    {
        this.props.setParentState({element: {...this.props.selectedItem, phone: e.target.value}});
    }
    onChangeEmailForm = (e) =>   {
        this.props.setParentState({element: {...this.props.selectedItem, email: e.target.value}});
    }

    save = () => {
        let idElement = this.props.selectedItem.id
        this.props.saveElement(idElement)
    }

    render () {
        return (
            <Modal {...this.props} aria-labelledby="contained-modal-title-vcenter">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        <h3>Создание/изменение контакта</h3>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="show-grid">
                    <Container>
                        <Row className="rowFormStyle">
                            <Col xs={8} md={4}>
                                <h6 className="colH6Style">Имя:</h6>
                            </Col>
                            <Col className="colStyle">
                                <Form.Control id="disabledTextInput"
                                              value={this.props.selectedItem ? this.props.selectedItem.name : ''}
                                              onChange={this.onChangeNameForm}/>
                            </Col>
                        </Row>
                        <Row className="rowFormStyle">
                            <Col xs={8} md={4}>
                                <h6 className="colH6Style">Фамилия:</h6>
                            </Col>
                            <Col className="colStyle">
                                <Form.Control id="disabledTextInput"
                                              value={this.props.selectedItem ? this.props.selectedItem.surname : ''}
                                              onChange={this.onChangeSurnameForm}/>
                            </Col>
                        </Row>
                        <Row className="rowFormStyle">
                            <Col xs={8} md={4}>
                                <h6 className="colH6Style">Телефон:</h6>
                            </Col>
                            <Col className="colStyle">
                                <Form.Control id="disabledTextInput"
                                              value={this.props.selectedItem ? this.props.selectedItem.phone : ''}
                                              onChange={this.onChangeTeleForm}/>
                            </Col>
                        </Row>
                        <Row className="rowFormStyle">
                            <Col xs={8} md={4}>
                                <h6 className="colH6Style">Email:</h6>
                            </Col>
                            <Col className="colStyle">
                                <Form.Control id="disabledTextInput"
                                              value={this.props.selectedItem ? this.props.selectedItem.email : ''}
                                              onChange={this.onChangeEmailForm}/>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.save}>Сохранить</Button>
                    <Button onClick={this.props.onHide}>Закрыть</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
export default ModalFormPersonal;

