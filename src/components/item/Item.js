import React from "react";
import {Button, CloseButton, Form} from "react-bootstrap";
import ModalFormPersonal from "./ModalFormPersonal";
import './Item.css';
import { BsFillPencilFill } from "react-icons/bs";
import logo from './favicon.ico'
import { Navigate } from "react-router-dom";
import {config} from "../../configs/mainconf";

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authorized: true,
            viewModal: false,
            element: {},
            findListItem: {name: '', surname: ''},
            listFind: [],
            changeItems: [],
            defaultItem: [],
            showError: false,
        }
    }

    componentDidMount() {
        const token = localStorage.getItem('token')
        if (token != null) {
            this.getData(token)
        } else {
            this.setState({authorized: false});
        }
    }

    getData = (token) => {
        let locData = localStorage.getItem('list');
        let locDataArray = JSON.parse(locData);
        if ((locData == null) || (locDataArray.length === 0)) { //Проверка на пустой localStorage
            fetch(config.host + '/contacts', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then((response) => {
                if (response.status === 401) {
                    this.setState({authorized: false});
                }
                return response.json()
            }).then((data) => {
                this.setState({authorized: true, defaultItem: data, changeItems: data});
                localStorage.setItem('list', JSON.stringify(data));
            })
        }
        else {
            this.setState({authorized: true, changeItems: JSON.parse(locData)});
        }
    }

    onHide = (view) => {
        this.setState({viewModal: view});
    }

    setParentState = (state={},callback) => {
        this.setState(state, callback);
    }

    onUpdateClick   = (e) => {
        let findId = e.target.dataset.id;
        let arrayMas = [...this.state.changeItems];
        let result = arrayMas.find( function (item) {
            return item.id == findId;
        });
        if (result) {
            this.setState({element: result, viewModal: true});
        }
    }

    saveElement = (id) => {
        let arrayMas = [...this.state.changeItems];
        if (id && id > 0) {
            for (let i = 0; i < arrayMas.length; i++) {
                if (Number(id) === Number(arrayMas[i].id)) {
                    arrayMas[i] = this.state.element;
                    break;
                }
            }
            this.setState({changeItems: arrayMas, viewModal: false}, function () {
                localStorage.setItem('list', JSON.stringify(this.state.changeItems))});
        }
        else {
            arrayMas.push({...this.state.element, id: Date.now()});
            this.setState({changeItems: arrayMas, viewModal: false}, function () {
                localStorage.setItem('list', JSON.stringify(this.state.changeItems))});
        }
    }

    buttonDelete = (e) => {
        let findId = e.target.dataset.id;
        let arrayMas = [...this.state.changeItems];
        for (let i = 0; i < arrayMas.length; i++) {
            if (Number(findId) === Number(arrayMas[i].id)) {
                arrayMas.splice(i,1);
                break;
            }
        }
        this.setState({changeItems: arrayMas}, function () {
            localStorage.setItem('list', JSON.stringify(this.state.changeItems))
        })
    }

    onFindNameForm = (e) =>    {
        this.setState({findListItem: {...this.state.findListItem, name: e.target.value}, showError: false});
    }
    onFindSurnameForm = (e) =>    {
        this.setState({findListItem: {...this.state.findListItem, surname: e.target.value}, showError: false});
    }

    findData = () =>   {
        let locData = localStorage.getItem('list');
        let locDataArray = JSON.parse(locData);
        let nameFined = String(this.state.findListItem.name);
        let surnameFined = String(this.state.findListItem.surname);

        if (nameFined === '' && surnameFined === '')    {
            this.onUpdateList();
        }
        else {
            let result = locDataArray.filter(function (itemEl) {
                return ((nameFined === itemEl.name && surnameFined === itemEl.surname) ||
                    (nameFined === itemEl.name && surnameFined === '') ||
                    (nameFined === '' && surnameFined === itemEl.surname))
                })
            if (result) {
                this.setState({showError: true});
            }

            this.setState({changeItems: result});
            localStorage.setItem('list', JSON.stringify(this.state.changeItems));
        }
    }

    onUpdateList = () => {
        const token = localStorage.getItem('token')
        if (token != null) {
            this.getData(token)
            fetch(config.host + '/contacts', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then((response) => {
                if (response.status === 401) {
                    this.setState({authorized: false});
                }
                return response.json()
            }).then((data) => {
                this.setState({changeItems: data, showError: false});
                localStorage.setItem('list', JSON.stringify(data));
            })
        }
    }

    clearConsole = () =>    {
        localStorage.clear();
        this.setState({authorized: false});
    }

    render () {
        if (!this.state.authorized) {
            return <Navigate to='/unregistered'/>;
        }
        else {
            return (
                <div>
                    <div className="wrapperFilter">
                        <div className="filter">
                            <Form className="formFilter">
                                <Form.Control className="inputName" size="sm" type="text"
                                              placeholder="Имя" onChange={this.onFindNameForm}/>
                                <Form.Control className="inputSurname" size="sm" type="text"
                                              placeholder="Фамилия" onChange={this.onFindSurnameForm}/>
                                <Button variant="primary" size="sm" onClick={this.findData}>Найти</Button>
                                <Button variant="primary" size="sm" className="btnUpdate"
                                        onClick={this.onUpdateList}>Обновить</Button>
                                    <Button onClick={() => {this.onHide(true)}}
                                            variant="warning" size="sm">Создать</Button>
                            </Form>

                            <Form className="wrapperBtnClose">
                                <Button variant="danger" size="sm" onClick={this.clearConsole}>Выйти</Button>
                            </Form>
                        </div>
                    </div>
                    <div className="blockElement">
                        {this.state.showError ? <h2>По данному запросу нет сохраненных контактов!</h2> : ''}
                        {this.state.changeItems ? this.state.changeItems.map(item => {
                            return(
                                <div className="element" key={item.id}>
                                    <div className="headerElement">
                                        <div className="logo">
                                            <img src={logo} alt=""/>
                                        </div>
                                        <div className="headerWrap">
                                            <div className="wrapName">
                                                <div className="headerName">
                                                    <h2>{item.name}</h2>
                                                </div>
                                                <div className="headerSurname">
                                                    <h2>{item.surname}</h2>
                                                </div>
                                            </div>
                                            <div className="btnDel">
                                                <div className="btnChange" data-id={item.id} onClick={this.onUpdateClick} >
                                                    <BsFillPencilFill data-id={item.id} onClick={this.onUpdateClick}/>
                                                </div>
                                                <CloseButton data-id={item.id} onClick={this.buttonDelete}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="wrapData">
                                        <div className="wrapDataInfo">
                                            <div className="dataElementPhone">
                                                <p>Телефон: </p>
                                                <p className="info">{item.phone}</p>
                                            </div>
                                            <div className="dataElementMail">
                                                <p>Email: </p>
                                                <p className="info">{item.email}</p>
                                            </div>
                                            <Form className="wrapperBtnChange">
                                            </Form>
                                        </div>
                                    </div>
                                    <ModalFormPersonal show={this.state.viewModal}
                                               onHide={() => {this.onHide(false)}}
                                               selectedItem={this.state.element}
                                               setParentState={this.setParentState}
                                               saveElement={this.saveElement}/>
                                </div>
                            )
                        }) : <div>Нет данных для отображения</div>}
                    </div>
                </div>
            )
        }
    }
}
export default Item;
