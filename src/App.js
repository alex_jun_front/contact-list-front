import React from "react";
import './App.css';
import Item from "./components/item/Item";
import Admin from "./components/loginPage/LoginPage";
import ModalFormPersonal from "./components/item/ModalFormPersonal";
import Unregistered from "./components/unlog/Unlog";


function App() {
  return (
          <div>
              <Admin/>
              <Item/>
              <ModalFormPersonal/>
              <Unregistered />
          </div>

  );
}

export default App;
