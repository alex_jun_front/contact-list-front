import React from "react";
import {Button, Form} from "react-bootstrap";
import './LoginPage.css'
import {Link, Navigate} from "react-router-dom";
import {config} from "../../configs/mainconf";

class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showError: false,
            defaultView: "",
            login: '',
            password: ''
        }
    }

    login = () => {
        this.sendLogin().then((data) => {
            console.log(data); // JSON data parsed by `response.json()` call
            if (data.success && data.token) {
                localStorage.setItem('token', data.token);
                this.setState({defaultView: "/item"});
            }
            else {
                this.setState({showError: true});
            }
        });
        // {success: false, message: 'Неверный логин или пароль'}
        // {success: true, user_id: 1, token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxM…DIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'}
    }

    sendLogin = async () => {
        const response = await fetch(config.host + '/login', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: this.state.login,
                password: this.state.password
            }) // body data type must match "Content-Type" header
        });
        return await response.json(); // parses JSON response into native JavaScript objects
    }

    onChangeLogin = (e) =>    {
        this.setState({login: e.target.value});
    }
    onChangePass = (e) =>    {
        this.setState({password: e.target.value});
    }

    render () {
        if (this.state.defaultView.length > 0) {
            return <Navigate to={this.state.defaultView}/>;
        } else {
            return (
                <div className="box">
                    <div className="wrapperForm">
                        <Form className="login">
                            <h3>Страница со списком контактов пользователя доступна только после авторизации</h3>
                            <Form.Group className="mb-3 formClass" controlId="formBasicEmail">
                                <Form.Label>Логин</Form.Label>
                                <Form.Control placeholder="Введите логин" onChange={this.onChangeLogin}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Пароль</Form.Label>
                                <Form.Control type="password" placeholder="Пароль" onChange={this.onChangePass}/>
                            </Form.Group>
                            {this.state.showError ? <p className="pError">Неверно введен логин или пароль!</p> : ''}
                            <Button variant="primary" onClick={this.login}>Войти</Button>
                        </Form>
                    </div>
                </div>
            );
        }
    }
}
export default Admin;





