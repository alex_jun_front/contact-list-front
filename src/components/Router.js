import * as React from "react";
import { Routes, Route } from "react-router-dom";
import Admin from "./loginPage/LoginPage";
import Item from "./item/Item";
import 'bootstrap/dist/css/bootstrap.min.css';
import Unregistered from "./unlog/Unlog";

function Router () {
    return (
        <Routes>
            <Route path="/" element={<Admin />} />
            <Route path="item" element={<Item />} />
            <Route path="unregistered" element={<Unregistered />} />
        </Routes>
    );
}
export default Router;